# Use the right Makefile for the system
# TODO: See if it's possible just to have one Makefile,
#       which might make maintenance easier.

UNAME := $(shell uname)
ifeq ($(UNAME),Linux)
  include Makefile.linux
else ifeq ($(UNAME),Darwin)
  include Makefile.mac
else
  $(error "Cannot yet build on $(UNAME)")
endif
